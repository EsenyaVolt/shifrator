﻿
// ShifratorDlg.h: файл заголовка
//

#pragma once

#include <string>

// Диалоговое окно CShifratorDlg
class CShifratorDlg : public CDialogEx
{
// Создание
public:
	CShifratorDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SHIFRATOR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedKluch();
	afx_msg void OnBnClickedZagruzka();
	afx_msg void OnBnClickedVypoln();
	afx_msg void OnBnClickedVygruzka();

	afx_msg void XORShifrator();
	afx_msg void XORRashifrator();
	afx_msg void DESShifrator();
	afx_msg void DESRashifrator();
	afx_msg void RSAShifrator();
	afx_msg void RSARashifrator();

	CString ish_text;
	CString shifr_text;
	CString key;
	CButton zashifr;
	CButton rasshifr;
	CButton ishod;
	CButton shifr;
	CButton new_txt;
	CButton shifrTXT;
	CButton XOR;
	CButton DES;
	CButton RSA;

	long long open_key;
	long long close_key;

	long long n_glob;
};
