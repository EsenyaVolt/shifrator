﻿
// ShifratorDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Shifrator.h"
#include "ShifratorDlg.h"
#include "afxdialogex.h"
#include <clocale>

#include <string.h>
#include <string>
#include <fstream>
#include <bitset>
#include <vector>
#include <math.h>
#include <conio.h>
#include <cassert>
#include <sstream>
#include <random>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

// Диалоговое окно CShifratorDlg

static int ip[64]{ 57, 49, 41, 33, 25, 17, 9, 1,
										59, 51, 43, 35, 27, 19, 11, 3,
										61, 53, 45, 37, 29, 21, 13, 5,
										63, 55, 47, 39, 31, 23, 15, 7,
										56, 48, 40, 32, 24, 16,  8, 0,
										58, 50, 42, 34, 26, 18, 10, 2,
										60, 52, 44, 36, 28, 20, 12, 4,
										62, 54, 46, 38, 30, 22, 14, 6 };

static int expansion[48]{ 31,  0,  1,  2,  3,  4,  3,  4,
										 5,  6,  7,  8,  7,  8, 9, 10,
										11, 12, 11, 12, 13, 14, 15, 16,
										15, 16, 17, 18, 19, 20, 19, 20,
										21, 22, 23, 24, 23, 24, 25, 26,
										27, 28, 27, 28, 29, 30, 31,  0 };

static byte s_block[8][4][16]
{
					   {{0x0e, 0x04, 0x0d, 0x01, 0x02, 0x0f, 0x0b, 0x08, 0x03, 0x0a, 0x06, 0x0c, 0x05, 0x09, 0x00, 0x07},
						{0x00, 0x0f, 0x07, 0x04, 0x0e, 0x02, 0x0d, 0x01, 0x0a, 0x06, 0x0c, 0x0b, 0x09, 0x05, 0x03, 0x08},
						{0x04, 0x01, 0x04, 0x08, 0x0d, 0x06, 0x02, 0x0b, 0x0f, 0x0c, 0x09, 0x07, 0x03, 0x0a, 0x05, 0x00},
						{0x0f, 0x0c, 0x08, 0x02, 0x04, 0x09, 0x01, 0x07, 0x05, 0x0b, 0x03, 0x0e, 0x0a, 0x00, 0x06, 0x0d}},
					   {{0x0f, 0x01, 0x08, 0x0e, 0x06, 0x0b, 0x03, 0x04, 0x09, 0x07, 0x02, 0x0d, 0x0c, 0x00, 0x05, 0x0a},
						{0x03, 0x0d, 0x04, 0x07, 0x0f, 0x02, 0x08, 0x0e, 0x0c, 0x00, 0x01, 0x0a, 0x06, 0x09, 0x0b, 0x05},
						{0x00, 0x0e, 0x07, 0x0b, 0x0a, 0x04, 0x0d, 0x01, 0x05, 0x08, 0x0c, 0x06, 0x09, 0x03, 0x02, 0x0f},
						{0x0d, 0x08, 0x0a, 0x01, 0x03, 0x0f, 0x04, 0x02, 0x0b, 0x06, 0x07, 0x0c, 0x00, 0x05, 0x0e, 0x09}},
					   {{0x0a, 0x00, 0x09, 0x0e, 0x06, 0x03, 0x0f, 0x05, 0x01, 0x0d, 0x0c, 0x07, 0x0b, 0x04, 0x02, 0x08},
						{0x0d, 0x07, 0x00, 0x09, 0x03, 0x04, 0x06, 0x0a, 0x02, 0x08, 0x05, 0x0e, 0x0c, 0x0b, 0x0f, 0x01},
						{0x0d, 0x06, 0x04, 0x09, 0x08, 0x0f, 0x03, 0x00, 0x0b, 0x01, 0x02, 0x0c, 0x05, 0x0a, 0x0e, 0x07},
						{0x01, 0x0a, 0x0d, 0x00, 0x06, 0x09, 0x08, 0x07, 0x04, 0x0f, 0x0e, 0x03, 0x0b, 0x05, 0x02, 0x0c}},
					   {{0x07, 0x0d, 0x0e, 0x03, 0x00, 0x06, 0x09, 0x0a, 0x01, 0x02, 0x08, 0x05, 0x0b, 0x0c, 0x04, 0x0f},
						{0x0d, 0x08, 0x0b, 0x05, 0x06, 0x0f, 0x00, 0x03, 0x04, 0x07, 0x02, 0x0c, 0x01, 0x0a, 0x0e, 0x09},
						{0x0a, 0x06, 0x09, 0x00, 0x0c, 0x0b, 0x07, 0x0d, 0x0f, 0x01, 0x03, 0x0e, 0x05, 0x02, 0x08, 0x04},
						{0x03, 0x0f, 0x00, 0x06, 0x0a, 0x01, 0x0d, 0x08, 0x09, 0x04, 0x05, 0x0b, 0x0c, 0x07, 0x02, 0x0e}},
					   {{0x02, 0x0c, 0x04, 0x01, 0x07, 0x0a, 0x0b, 0x06, 0x08, 0x05, 0x03, 0x0f, 0x0d, 0x00, 0x0e, 0x09},
						{0x0e, 0x0b, 0x02, 0x0c, 0x04, 0x07, 0x0d, 0x01, 0x05, 0x00, 0x0f, 0x0a, 0x03, 0x09, 0x08, 0x06},
						{0x04, 0x02, 0x01, 0x0b, 0x0a, 0x0d, 0x07, 0x08, 0x0f, 0x09, 0x0c, 0x05, 0x06, 0x03, 0x00, 0x0e},
						{0x0b, 0x08, 0x0c, 0x07, 0x01, 0x0e, 0x02, 0x0d, 0x06, 0x0f, 0x00, 0x09, 0x0a, 0x04, 0x05, 0x03}},
					   {{0x0c, 0x01, 0x0a, 0x0f, 0x09, 0x02, 0x06, 0x08, 0x00, 0x0d, 0x03, 0x04, 0x0e, 0x07, 0x05, 0x0b},
						{0x0a, 0x0f, 0x04, 0x02, 0x07, 0x0c, 0x09, 0x05, 0x06, 0x01, 0x0d, 0x0e, 0x00, 0x0b, 0x03, 0x08},
						{0x09, 0x0e, 0x0f, 0x05, 0x02, 0x08, 0x0c, 0x03, 0x07, 0x00, 0x04, 0x0a, 0x01, 0x0d, 0x01, 0x06},
						{0x04, 0x03, 0x02, 0x0c, 0x09, 0x05, 0x0f, 0x0a, 0x0b, 0x0e, 0x01, 0x07, 0x06, 0x00, 0x08, 0x0d}},
					   {{0x04, 0x0b, 0x02, 0x0e, 0x0f, 0x00, 0x08, 0x0d, 0x03, 0x0c, 0x09, 0x07, 0x05, 0x0a, 0x06, 0x01},
						{0x0d, 0x00, 0x0b, 0x07, 0x04, 0x09, 0x01, 0x0a, 0x0e, 0x03, 0x05, 0x0c, 0x02, 0x0f, 0x08, 0x06},
						{0x01, 0x04, 0x0b, 0x0d, 0x0c, 0x03, 0x07, 0x0e, 0x0a, 0x0f, 0x06, 0x08, 0x00, 0x05, 0x09, 0x02},
						{0x06, 0x0b, 0x0d, 0x08, 0x01, 0x04, 0x0a, 0x07, 0x09, 0x05, 0x00, 0x0f, 0x0e, 0x02, 0x03, 0x0c}},
					   {{0x0d, 0x02, 0x08, 0x04, 0x06, 0x0f, 0x0b, 0x01, 0x0a, 0x09, 0x03, 0x0e, 0x05, 0x00, 0x0c, 0x07},
						{0x01, 0x0f, 0x0d, 0x08, 0x0a, 0x03, 0x07, 0x04, 0x0c, 0x05, 0x06, 0x0b, 0x00, 0x0e, 0x09, 0x02},
						{0x07, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x0e, 0x02, 0x00, 0x06, 0x0a, 0x0d, 0x0f, 0x03, 0x05, 0x08},
						{0x02, 0x01, 0x0e, 0x07, 0x04, 0x0a, 0x08, 0x0d, 0x0f, 0x0c, 0x09, 0x00, 0x03, 0x05, 0x06, 0x0b}}
};

static int permutation_func[32]{ 15,  6, 19, 20, 28, 11, 27, 16,
												 0, 14, 22, 25,  4, 18, 30, 9,
												 1,  7, 23, 13, 31, 26,  2, 8,
												18, 12, 29,  5, 21, 10,  3, 24 };

static int final_permutation[64]{ 39, 7, 47, 15, 55, 23, 63, 31,
												38, 6, 46, 14, 54, 22, 62, 30,
												37, 5, 45, 13, 53, 21, 61, 29,
												36, 4, 44, 12, 52, 20, 60, 28,
												35, 3, 43, 11, 51, 19, 59, 27,
												34, 2, 42, 10, 50, 18, 58, 26,
												33, 1, 41,  9, 49, 17, 57, 25,
												32, 0, 40,  8, 48, 16, 56, 24 };

static int C_block[28]{ 56, 48, 40, 32, 24, 16, 8,
													0, 57, 49, 41, 33, 25, 17,
													9, 1, 58, 50, 42, 34, 26,
													18, 10, 2, 59, 51, 43, 35 };

static int D_block[28]{ 62, 54, 46, 38, 30, 22, 14,
													6, 61, 53, 45, 37, 29, 21,
													13, 5, 60, 52, 44, 36, 28,
													20, 12, 4, 27, 19, 11, 3 };

static int key_block[48]{ 13, 16, 10, 23, 0, 4,
											2, 27, 14, 5,  20,  9,
											22, 18, 11, 3, 25, 7,
											15, 6, 26, 19, 12, 1,
											40, 51, 30, 36, 46, 54,
											29, 39, 50, 44, 32, 47,
											43, 48, 38, 55, 33, 52,
											45, 41, 49, 35, 28, 31 };

static int key_shift[16]{ 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };


string help_text;

string IP_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 64; i++)
	{
		res += s[ip[i]];
	}
	return res;
}

string E_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 48; i++)
	{
		res += s[expansion[i]];
	}
	return res;
}

string C_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 28; i++)
	{
		res += s[C_block[i]];
	}
	return res;
}

string D_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 28; i++)
	{
		res += s[D_block[i]];
	}
	return res;
}

string CD_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 48; i++)
	{
		res += s[key_block[i]];
	}
	return res;
}

string P_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 32; i++)
	{
		res += s[permutation_func[i]];
	}
	return res;
}

string IP_MINUS_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 64; i++)
	{
		res += s[final_permutation[i]];
	}
	return res;
}

string XORf(string s1, string s2)
{
	string result = "";

	for (int i = 0; i < s1.length(); i++)
	{
		if (s1[i] == s2[i])
		{
			result += '0';
		}
		else
		{
			result += '1';
		}
	}
	return result;
}

string f(string R, string k)
{
	string expanshion_r = E_METHOD(R);
	string pred_s_block = XORf(expanshion_r, k);
	string pred_s_block_to_block[8];

	for (int i = 0; i < 8; i++)
	{
		pred_s_block_to_block[i] = pred_s_block.substr(i * 6, 6);
	}

	string NumberOfString[8];
	string NumberOfColumn[8];
	int NumberOfStringHex[8]{ 0 };
	int NumberOfColumnHex[8]{ 0 };
	int S_block_number_int[8]{ 0 };
	string S_block_number_str[8];
	string S_block_number_bin[8];
	string S_block_out = "";
	string S_block_out_reverse = "";

	for (int i = 0; i < 8; i++)
	{
		NumberOfString[i] += pred_s_block_to_block[i][0];
		NumberOfString[i] += pred_s_block_to_block[i][5];

		NumberOfColumn[i] += pred_s_block_to_block[i][1];
		NumberOfColumn[i] += pred_s_block_to_block[i][2];
		NumberOfColumn[i] += pred_s_block_to_block[i][3];
		NumberOfColumn[i] += pred_s_block_to_block[i][4];

		for (int j = 0; j < NumberOfString[i].length(); j++)
		{
			NumberOfStringHex[i] *= 2;
			NumberOfStringHex[i] += NumberOfString[i][j] - '0';
		}

		for (int j = 0; j < NumberOfColumn[i].length(); j++)
		{
			NumberOfColumnHex[i] *= 2;
			NumberOfColumnHex[i] += NumberOfColumn[i][j] - '0';
		}

		S_block_number_int[i] = s_block[i][NumberOfStringHex[i]][NumberOfColumnHex[i]];
		S_block_number_str[i] = S_block_number_int[i];


		for (int c : S_block_number_str[i])
		{
			bitset<4> bs(c);
			S_block_number_bin[i] += bs.to_string();
			S_block_out += S_block_number_bin[i];
		}
	}

	S_block_out_reverse = P_METHOD(S_block_out);

	return S_block_out_reverse;
}

string StringFromBinaryToNormalFormat(string input)
{
	string output = "";
	string* Blocks = new string[input.length() / 8];

	for (int i = 0; i < input.length() / 8; i++)
	{
		Blocks[i]= input.substr(i * 8, 8);
	}

	for (int i = 0; i < input.length() / 8; i++)
	{
		bitset<8> str(Blocks[i]);
		unsigned char ch = static_cast <unsigned char> (str.to_ulong());
		output += ch;
	}
	
	delete[] Blocks;
	return output;
}

CShifratorDlg::CShifratorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SHIFRATOR_DIALOG, pParent)
	, key(_T(""))
	, ish_text(_T(""))
	, shifr_text(_T(""))
	, open_key(0)
	, close_key(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CShifratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_KEY, key);
	DDX_Text(pDX, IDC_NESHYFR, ish_text);
	DDX_Text(pDX, IDC_SHYFR, shifr_text);
	DDX_Control(pDX, IDC_SHIFR, zashifr);
	DDX_Control(pDX, IDC_RASHIFR, rasshifr);
	DDX_Control(pDX, IDC_SHIFR3, shifr);
	DDX_Control(pDX, IDC_RASHIFR3, ishod);
	DDX_Control(pDX, IDC_RASHIFR2, new_txt);
	DDX_Control(pDX, IDC_SHIFR2, shifrTXT);
	DDX_Control(pDX, IDC_XOR, XOR);
	DDX_Control(pDX, IDC_DES, DES);
	DDX_Control(pDX, IDC_RSA, RSA);
	DDX_Text(pDX, IDC_KEY2, open_key);
	DDX_Text(pDX, IDC_KEY3, close_key);
}

BEGIN_MESSAGE_MAP(CShifratorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_KLUCH, &CShifratorDlg::OnBnClickedKluch)
	ON_BN_CLICKED(IDC_ZAGRUZKA, &CShifratorDlg::OnBnClickedZagruzka)
	ON_BN_CLICKED(IDC_VYPOLN, &CShifratorDlg::OnBnClickedVypoln)
	ON_BN_CLICKED(IDC_VYGRUZKA, &CShifratorDlg::OnBnClickedVygruzka)
END_MESSAGE_MAP()


// Обработчики сообщений CShifratorDlg

BOOL CShifratorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CShifratorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CShifratorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CShifratorDlg::XORShifrator()
{
	CString txt = ish_text;
	CString Key = key;

	string str;
	str.resize(txt.GetLength());
	WideCharToMultiByte(CP_ACP, 0, txt, -1, &str[0], str.size(), NULL, NULL);

	string helpkey;
	helpkey.resize(Key.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Key, -1, &helpkey[0], helpkey.size(), NULL, NULL);

	string rezult;
	rezult.resize(txt.GetLength());

	int ost = 0;
	ost = str.length() % helpkey.length();

	int addLength = helpkey.length() - ost;

	int newLength = str.length() + addLength;

	if (ost == 0)
	{
		for (int i = 0; i < str.length(); i++)
		{
			rezult[i] = str[i] ^ helpkey[i % helpkey.length()];
		}
	}
	else
	{
		str.resize(newLength, '0');
		rezult.resize(str.length());

		for (int i = 0; i < newLength; i++)
		{
			rezult[i] = str[i] ^ helpkey[i % helpkey.length()];
		}
	}

	shifr_text = rezult.c_str();

	UpdateData(FALSE);
}

void CShifratorDlg::XORRashifrator()
{
	CString shifr_txt = shifr_text;
	CString Key = key;

	string str;
	str.resize(shifr_txt.GetLength());
	WideCharToMultiByte(CP_ACP, 0, shifr_txt, -1, &str[0], str.size(), NULL, NULL);

	string helpkey;
	helpkey.resize(Key.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Key, -1, &helpkey[0], helpkey.size(), NULL, NULL);

	string rezult;
	rezult.resize(shifr_txt.GetLength());

	for (int i = 0; i < str.length(); i++)
	{
		rezult[i] = str[i] ^ helpkey[i % helpkey.length()];
	}

	for (int i = str.length(); i > str.length() - helpkey.length(); i--)
	{
		if (rezult[i] == '0')
		{
			rezult.erase(i, 1);
		}
	}

	ish_text = rezult.c_str();

	UpdateData(FALSE);
}

void CShifratorDlg::DESShifrator()
{
	UpdateData(TRUE);
	if (key.GetLength() == NULL) MessageBox(L"Ошибка! Введите или сгенерируйте ключ!", L"ERROR", MB_OK | MB_ICONERROR);

	CString Text = ish_text;
	CString Key = key;

	string data;
	data.resize(Text.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Text, -1, &data[0], data.size(), NULL, NULL);

	string keyhelp;
	keyhelp.resize(Key.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Key, -1, &keyhelp[0], keyhelp.size(), NULL, NULL);

	// перевод в бинарный код каждого символа ключа (8 символов = 64 бита)

	int sum, ostatok;
	ofstream out("bits_shifr.txt");
	out << "Сгенерированный ключ: " << keyhelp << '\n' << '\n';

	string BinaryKey = "";

	for (char c : keyhelp)		//цикл, пробегающий по каждому символу ключа и переводящий в двоичный код
	{
		sum = 0;
		ostatok = 0;

		bitset<8> bs(c);		//непосредственно перевод в двоичный код (8 бит)
		string bitkey56 = "";
		bitkey56 = bs.to_string();		//записываем в строку

		for (int i = 0; i < bitkey56.length() - 1; i++)
		{
			sum += bitkey56[i];		//объединяем результат каждой буквы и получается 64 бит ключа
		}
		ostatok = sum % 2;		//смотрим четность первых 7 бит, чтобы понять, каким нужно сделать последний 8ой бит
		
		if (ostatok == 0)
		{
			if (bitkey56.back() != '1')	bitkey56.back() = '1';
		}
		BinaryKey += bitkey56;

		out << c << " : " << bitkey56 << '\n';
	}
	out << '\n' << "Ключ в двоичном коде: " << BinaryKey << '\n' << '\n';

	out << '\n' << "Длина текста до дополнения: " << data.length() << '\n' << '\n';

	int dif_before = data.length() % keyhelp.length();		//дополняем исходный текст до кратного ключу(как в предыдущем случае шифрования)
	if (dif_before != 0)
	{
		int additionLength = keyhelp.length() - dif_before;
		int NewLen = data.length() + additionLength;
		data.resize(NewLen, '0');
	}

	out << data.c_str() << '\n' << '\n' << "Длина текста после дополнения: " << data.length() << '\n' << '\n';

	// алгоритм DES

	const int SizeOfBlock = 64;		//длина блока в двоич.
	const int SizeOfChar = 8;		//длина символа в двоич.
	const int QuantityOfRounds = 16;		//число итераций цикла фейстеля

	string BinaryData = "";
	for (char c : data)		//аналогия для текста
	{
		bitset<8> bs(c);
		BinaryData += bs.to_string();
	}

	int NewSizeOfBlock = BinaryData.length() / SizeOfBlock;

	out << "Длина текста в двоичном формате: " << BinaryData.length() << '\n' << '\n';

	string* Blocks = new string[NewSizeOfBlock];		//вводим массив блоков (делим текст по блокам)
	int LengthOfBlock = BinaryData.length() / NewSizeOfBlock;

	out << "Длина блока: " << LengthOfBlock << '\n' << '\n' << "Количество блоков: " << NewSizeOfBlock << '\n' << '\n';

	for (int i = 0; i < NewSizeOfBlock; i++)
	{
		Blocks[i] = BinaryData.substr(i * LengthOfBlock, LengthOfBlock);		//substr - копирование части строки
	}

	out << "Текст в двоичном представлении: " << '\n' << '\n';
	
	for (int i = 0; i < NewSizeOfBlock; i++)
	{
		out << Blocks[i];
	}

	//начальная перестановка
	string StartRevers = IP_METHOD(Blocks[0]);		//начальная перестановка (все методы вверху)

	string L = Blocks[0].substr(0, LengthOfBlock / 2);		//здесь идет не сам алгоритм, а просто проверка на работоспособность,
	string R = Blocks[0].substr(LengthOfBlock / 2, LengthOfBlock / 2);			//поэтому можно пропускать все, что не используется в самом алгоритме
	string res = XORf(L, R);

	out << '\n' << '\n' << "Исходный блок: " << Blocks[0] << '\n' << '\n' << "Блок после начальной перестановки: " << StartRevers << '\n' << '\n';

	out << "Левая часть: " << L << '\n' << "Правая часть: " << R << '\n' << "XOR: " << res << '\n' << '\n';

	//расширяем R
	string expanshion_r = E_METHOD(R);

	out << "Правая часть: " << R << '\n' << "Расширенная правая часть: " << expanshion_r << '\n' << '\n';

	//значение ключа 56 бит
	string key56 = "";
	for (int i = 0; i < BinaryKey.length(); i++)
	{
		if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63)
		{
			key56 += BinaryKey[i];
		}
	}

	out << "Ключ 64 бита: " << BinaryKey << '\n' << "Ключ 56 бит: " << key56 << '\n' << '\n';

	string C = key56.substr(0, key56.length() / 2);
	string D = key56.substr(key56.length() / 2, key56.length() / 2);

	//перестановка C, D блоков

	//этот участок понадобится, это C,D блоки для нахождения функции f (можно посмотреть в документе инфу и привести аналогию)

	string C_reverse[QuantityOfRounds + 1];
	string D_reverse[QuantityOfRounds + 1];
	string C0_reverse = C_METHOD(BinaryKey);
	string D0_reverse = D_METHOD(BinaryKey);
	string CD0 = C0_reverse + D0_reverse;

	C_reverse[0] = C0_reverse;
	D_reverse[0] = D0_reverse;
	string buf1;
	string buf2;

	for (int i = 1; i <= QuantityOfRounds; i++)
	{
		if ((i == 1) || (i == 2) || (i == 9) || (i == 16))
		{
			buf1 = "";
			buf1 = C_reverse[i - 1][0];
			C_reverse[i] = C_reverse[i - 1] + buf1;
			C_reverse[i] = C_reverse[i].erase(0, 1);

			buf2 = "";
			buf2 = D_reverse[i - 1][0];
			D_reverse[i] = D_reverse[i - 1] + buf2;
			D_reverse[i] = D_reverse[i].erase(0, 1);
		}
		else
		{
			buf1 = "";
			buf1 += C_reverse[i - 1][0];
			buf1 += C_reverse[i - 1][1];
			C_reverse[i] = C_reverse[i - 1] + buf1;
			C_reverse[i] = C_reverse[i].erase(0, 2);

			buf2 = "";
			buf2 += D_reverse[i - 1][0];
			buf2 += D_reverse[i - 1][1];
			D_reverse[i] = D_reverse[i - 1] + buf2;
			D_reverse[i] = D_reverse[i].erase(0, 2);
		}
	}

	out << "C0-блок: " << C0_reverse << '\n' << "D0-блок: " << D0_reverse << '\n' << '\n';

	for (int i = 0; i <= QuantityOfRounds; i++)
	{
		out << "C-блок на " << i << "-ой итерации: " << '\t' << C_reverse[i] << '\n';// << "С-блок после перестановки: " << D_reverse[i] << '\n';
	}

	out << '\n';

	for (int i = 0; i <= QuantityOfRounds; i++)
	{
		out << "D-блок на " << i << "-ой итерации: " << '\t' << D_reverse[i] << '\n';// << "D-блок после перестановки: " << D_reverse[i] << '\n';
	}

	out << '\n';
	//ключ из 56 бит в 48 бит

	string CD[QuantityOfRounds];
	string key48[QuantityOfRounds];
	for (int j = 0; j < QuantityOfRounds; j++)
	{
		CD[j] = C_reverse[j + 1] + D_reverse[j + 1];
		key48[j] = CD_METHOD(CD[j]);
		out << "СD-блок на " << j << "-ой итерации: " << '\t' << CD[j] << '\n';
	}

	out << '\n';

	for (int i = 0; i < QuantityOfRounds; i++)
	{
		out << "Ключ на " << i << "-ой итерации:" << '\t' << key48[i] << '\n';
	}

	//нахождение функции f
	//std::string key48 = CD_METHOD(CD0);

	string pred_s_block = XORf(expanshion_r, key48[0]);
	string pred_s_block_to_block[8];

	for (int i = 0; i < 8; i++)
	{
		pred_s_block_to_block[i] = pred_s_block.substr(i * 6, 6);
	}

	out << '\n' << "Входная последовательно S-блоков: " << pred_s_block << '\n' << "Вход 1S-блока: " << pred_s_block_to_block[0] << '\n' << '\n';
	out << pred_s_block_to_block[0][0] << pred_s_block_to_block[0][5] << '\n' << '\n';

	string NumberOfString[8];
	string NumberOfColumn[8];
	int NumberOfStringHex[8]{ 0 };
	int NumberOfColumnHex[8]{ 0 };
	int S_block_number_int[8]{ 0 };
	string S_block_number_str[8];
	string S_block_number_bin[8];
	string S_block_out = "";

	for (int i = 0; i < 8; i++)
	{
		NumberOfString[i] += pred_s_block_to_block[i][0];
		NumberOfString[i] += pred_s_block_to_block[i][5];

		NumberOfColumn[i] += pred_s_block_to_block[i][1];
		NumberOfColumn[i] += pred_s_block_to_block[i][2];
		NumberOfColumn[i] += pred_s_block_to_block[i][3];
		NumberOfColumn[i] += pred_s_block_to_block[i][4];

		for (int j = 0; j < NumberOfString[i].length(); j++)
		{
			NumberOfStringHex[i] *= 2;
			NumberOfStringHex[i] += NumberOfString[i][j] - '0';
		}

		for (int j = 0; j < NumberOfColumn[i].length(); j++)
		{
			NumberOfColumnHex[i] *= 2;
			NumberOfColumnHex[i] += NumberOfColumn[i][j] - '0';
		}

		S_block_number_int[i] = s_block[i][NumberOfStringHex[i]][NumberOfColumnHex[i]];
		S_block_number_str[i] = S_block_number_int[i];


		for (int c : S_block_number_str[i])
		{
			bitset<4> bs(c);
			S_block_number_bin[i] += bs.to_string();
			S_block_out += S_block_number_bin[i];
		}
	}

	out << "Номер строки 1S-блока: " << NumberOfString[0] << '\n' << "Номер столбца 1S-блока: " << NumberOfColumn[0] << '\n' << '\n';
	out << "Номер строки 1S-блока (HEX) : " << NumberOfStringHex[0] << '\n' << "Номер столбца 1S-блока (HEX) : " << NumberOfColumnHex[0] << '\n' << '\n';
	out << "Число для 1S-блока: " << S_block_number_int[0] << '\n' << "Число для 1S-блока в двоичной системе: " << S_block_number_bin[0] << '\n' << '\n';
	out << "Выход S-блоков: " << S_block_out << '\n' << '\n';

	string S_block_out_reverse = P_METHOD(S_block_out);

	//алгоритм Фейстеля
	//вот непосредственно сам алгоритм, сам по себе небольшой

	string StartReversBlocks = "";
	string L0 = "";
	string R0 = "";
	string L_part[QuantityOfRounds];
	string R_part[QuantityOfRounds];
	string LR = "";
	string LR_revers;
	string ResultOfFeistel = "";

	//шифрование
	for (int j = 0; j < NewSizeOfBlock; j++)
	{

		StartReversBlocks = IP_METHOD(Blocks[j]);
		L0 = StartReversBlocks.substr(0, LengthOfBlock / 2);
		R0 = StartReversBlocks.substr(LengthOfBlock / 2, LengthOfBlock / 2);

		out << '\n' << j << "-ая итерация." << '\n' << "L0: " << L0 << '\n' << "R0: " << R0 << '\n';

		for (int i = 0; i < QuantityOfRounds; i++)
		{
			if (i == 0)
			{
				L_part[i] = R0;
				R_part[i] = XORf(L0, f(R0, key48[i]));
			}
			else
			{
				L_part[i] = R_part[i - 1];
				R_part[i] = XORf(L_part[i - 1], f(R_part[i - 1], key48[i]));
			}
		}

		LR = "";
		LR_revers = "";
		LR = R_part[15] + L_part[15];
		LR_revers = IP_MINUS_METHOD(LR);

		out << '\n' << "Результат для первого блока текста: " << LR_revers << '\n';

		ResultOfFeistel += LR_revers;
	}

	string exit = StringFromBinaryToNormalFormat(ResultOfFeistel);

	
	help_text = exit;
	shifr_text = (CString)help_text.c_str();
	UpdateData(FALSE);

	out << '\n' << "Результат шифрования методом DES (BIN): " << '\n' << ResultOfFeistel << '\n' << '\n';
	out << "Длина зашифрованного текста в двоичном виде: " << ResultOfFeistel.length() << '\n' << '\n';
	out << "Результат шифрования методом DES (NORMAL): " << '\n' << exit << '\n' << '\n';
	out << "Длина зашифрованного текста в нормальном виде: " << exit.length() << '\n' << '\n';

	//delete Blocks;
	out.close();
}

void CShifratorDlg::DESRashifrator()
{
	UpdateData(TRUE);
	if (key.GetLength() == NULL) MessageBox(L"Ошибка! Введите или сгенерируйте ключ!", L"ERROR", MB_OK | MB_ICONERROR);

	CString Key = key;
	string data = help_text;

	string keyhelp;
	keyhelp.resize(Key.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Key, -1, &keyhelp[0], keyhelp.size(), NULL, NULL);

	// перевод в бинарный код каждого символа ключа (8 символов = 64 бита)

	int sum, ostatok;
	ofstream out("rasshifr.txt");
	out << "Сгенерированный ключ: " << keyhelp << '\n' << '\n';

	string BinaryKey = "";

	for (char c : keyhelp)		//цикл, пробегающий по каждому символу ключа и переводящий в двоичный код
	{
		sum = 0;
		ostatok = 0;

		bitset<8> bs(c);		//непосредственно перевод в двоичный код (8 бит)
		string bitkey56 = "";
		bitkey56 = bs.to_string();		//записываем в строку

		for (int i = 0; i < bitkey56.length() - 1; i++)
		{
			sum += bitkey56[i];		//объединяем результат каждой буквы и получается 64 бит ключа
		}
		ostatok = sum % 2;		//смотрим четность первых 7 бит, чтобы понять, каким нужно сделать последний 8ой бит

		if (ostatok == 0)
		{
			if (bitkey56.back() != '1')	bitkey56.back() = '1';
		}
		BinaryKey += bitkey56;

		out << c << " : " << bitkey56 << '\n';
	}
	out << '\n' << "Ключ в двоичном коде: " << BinaryKey << '\n' << '\n';

	out << '\n' << "Длина текста до дополнения: " << data.length() << '\n' << '\n';

	int dif_before = data.length() % keyhelp.length();		//дополняем исходный текст до кратного ключу(как в предыдущем случае шифрования)
	if (dif_before != 0)
	{
		int additionLength = keyhelp.length() - dif_before;
		int NewLen = data.length() + additionLength;
		data.resize(NewLen, '0');
	}

	out << data << '\n' << '\n' << "Длина текста после дополнения: " << data.length() << '\n' << '\n';

	// алгоритм DES

	const int SizeOfBlock = 64;		//длина блока в двоич.
	const int SizeOfChar = 8;		//длина символа в двоич.
	const int QuantityOfRounds = 16;		//число итераций цикла фейстеля

	string BinaryData = "";
	for (char c : data)		//аналогия для текста
	{
		bitset<8> bs(c);
		BinaryData += bs.to_string();
	}

	int NewSizeOfBlock = BinaryData.length() / SizeOfBlock;

	out << "Длина текста в двоичном формате: " << BinaryData.length() << '\n' << '\n';

	string* Blocks = new string[NewSizeOfBlock];		//вводим массив блоков (делим текст по блокам)
	int LengthOfBlock = BinaryData.length() / NewSizeOfBlock;

	out << "Длина блока: " << LengthOfBlock << '\n' << '\n' << "Количество блоков: " << NewSizeOfBlock << '\n' << '\n';

	for (int i = 0; i < NewSizeOfBlock; i++)
	{
		Blocks[i] = BinaryData.substr(i * LengthOfBlock, LengthOfBlock);		//substr - копирование части строки
	}

	out << "Текст в двоичном представлении: " << '\n' << '\n';

	for (int i = 0; i < NewSizeOfBlock; i++)
	{
		out << Blocks[i];
	}

	//начальная перестановка
	string StartRevers = IP_METHOD(Blocks[0]);		//начальная перестановка (все методы вверху)

	string L = Blocks[0].substr(0, LengthOfBlock / 2);		//здесь идет не сам алгоритм, а просто проверка на работоспособность,
	string R = Blocks[0].substr(LengthOfBlock / 2, LengthOfBlock / 2);			//поэтому можно пропускать все, что не используется в самом алгоритме
	string res = XORf(L, R);

	out << '\n' << '\n' << "Исходный блок: " << Blocks[0] << '\n' << '\n' << "Блок после начальной перестановки: " << StartRevers << '\n' << '\n';

	out << "Левая часть: " << L << '\n' << "Правая часть: " << R << '\n' << "XOR: " << res << '\n' << '\n';

	//расширяем R
	string expanshion_r = E_METHOD(R);

	out << "Правая часть: " << R << '\n' << "Расширенная правая часть: " << expanshion_r << '\n' << '\n';

	//значение ключа 56 бит
	string key56 = "";
	for (int i = 0; i < BinaryKey.length(); i++)
	{
		if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63)
		{
			key56 += BinaryKey[i];
		}
	}

	out << "Ключ 64 бита: " << BinaryKey << '\n' << "Ключ 56 бит: " << key56 << '\n' << '\n';

	string C = key56.substr(0, key56.length() / 2);
	string D = key56.substr(key56.length() / 2, key56.length() / 2);

	//перестановка C, D блоков

	//этот участок понадобится, это C,D блоки для нахождения функции f (можно посмотреть в документе инфу и привести аналогию)

	string C_reverse[QuantityOfRounds + 1];
	string D_reverse[QuantityOfRounds + 1];
	string C0_reverse = C_METHOD(BinaryKey);
	string D0_reverse = D_METHOD(BinaryKey);
	string CD0 = C0_reverse + D0_reverse;

	C_reverse[0] = C0_reverse;
	D_reverse[0] = D0_reverse;
	string buf1;
	string buf2;

	for (int i = 1; i <= QuantityOfRounds; i++)
	{
		if ((i == 1) || (i == 2) || (i == 9) || (i == 16))
		{
			buf1 = "";
			buf1 = C_reverse[i - 1][0];
			C_reverse[i] = C_reverse[i - 1] + buf1;
			C_reverse[i] = C_reverse[i].erase(0, 1);

			buf2 = "";
			buf2 = D_reverse[i - 1][0];
			D_reverse[i] = D_reverse[i - 1] + buf2;
			D_reverse[i] = D_reverse[i].erase(0, 1);
		}
		else
		{
			buf1 = "";
			buf1 += C_reverse[i - 1][0];
			buf1 += C_reverse[i - 1][1];
			C_reverse[i] = C_reverse[i - 1] + buf1;
			C_reverse[i] = C_reverse[i].erase(0, 2);

			buf2 = "";
			buf2 += D_reverse[i - 1][0];
			buf2 += D_reverse[i - 1][1];
			D_reverse[i] = D_reverse[i - 1] + buf2;
			D_reverse[i] = D_reverse[i].erase(0, 2);
		}
	}

	out << "C0-блок: " << C0_reverse << '\n' << "D0-блок: " << D0_reverse << '\n' << '\n';

	for (int i = 0; i <= QuantityOfRounds; i++)
	{
		out << "C-блок на " << i << "-ой итерации: " << '\t' << C_reverse[i] << '\n';// << "С-блок после перестановки: " << D_reverse[i] << '\n';
	}

	out << '\n';

	for (int i = 0; i <= QuantityOfRounds; i++)
	{
		out << "D-блок на " << i << "-ой итерации: " << '\t' << D_reverse[i] << '\n';// << "D-блок после перестановки: " << D_reverse[i] << '\n';
	}

	out << '\n';
	//ключ из 56 бит в 48 бит

	string CD[QuantityOfRounds];
	string key48[QuantityOfRounds];
	for (int j = 0; j < QuantityOfRounds; j++)
	{
		CD[j] = C_reverse[j + 1] + D_reverse[j + 1];
		key48[j] = CD_METHOD(CD[j]);
		out << "СD-блок на " << j << "-ой итерации: " << '\t' << CD[j] << '\n';
	}

	out << '\n';

	for (int i = 0; i < QuantityOfRounds; i++)
	{
		out << "Ключ на " << i << "-ой итерации:" << '\t' << key48[i] << '\n';
	}

	//нахождение функции f
	//std::string key48 = CD_METHOD(CD0);

	string pred_s_block = XORf(expanshion_r, key48[0]);
	string pred_s_block_to_block[8];

	for (int i = 0; i < 8; i++)
	{
		pred_s_block_to_block[i] = pred_s_block.substr(i * 6, 6);
	}

	out << '\n' << "Входная последовательно S-блоков: " << pred_s_block << '\n' << "Вход 1S-блока: " << pred_s_block_to_block[0] << '\n' << '\n';
	out << pred_s_block_to_block[0][0] << pred_s_block_to_block[0][5] << '\n' << '\n';

	string NumberOfString[8];
	string NumberOfColumn[8];
	int NumberOfStringHex[8]{ 0 };
	int NumberOfColumnHex[8]{ 0 };
	int S_block_number_int[8]{ 0 };
	string S_block_number_str[8];
	string S_block_number_bin[8];
	string S_block_out = "";

	for (int i = 0; i < 8; i++)
	{
		NumberOfString[i] += pred_s_block_to_block[i][0];
		NumberOfString[i] += pred_s_block_to_block[i][5];

		NumberOfColumn[i] += pred_s_block_to_block[i][1];
		NumberOfColumn[i] += pred_s_block_to_block[i][2];
		NumberOfColumn[i] += pred_s_block_to_block[i][3];
		NumberOfColumn[i] += pred_s_block_to_block[i][4];

		for (int j = 0; j < NumberOfString[i].length(); j++)
		{
			NumberOfStringHex[i] *= 2;
			NumberOfStringHex[i] += NumberOfString[i][j] - '0';
		}

		for (int j = 0; j < NumberOfColumn[i].length(); j++)
		{
			NumberOfColumnHex[i] *= 2;
			NumberOfColumnHex[i] += NumberOfColumn[i][j] - '0';
		}

		S_block_number_int[i] = s_block[i][NumberOfStringHex[i]][NumberOfColumnHex[i]];
		S_block_number_str[i] = S_block_number_int[i];


		for (int c : S_block_number_str[i])
		{
			bitset<4> bs(c);
			S_block_number_bin[i] += bs.to_string();
			S_block_out += S_block_number_bin[i];
		}
	}

	out << "Номер строки 1S-блока: " << NumberOfString[0] << '\n' << "Номер столбца 1S-блока: " << NumberOfColumn[0] << '\n' << '\n';
	out << "Номер строки 1S-блока (HEX) : " << NumberOfStringHex[0] << '\n' << "Номер столбца 1S-блока (HEX) : " << NumberOfColumnHex[0] << '\n' << '\n';
	out << "Число для 1S-блока: " << S_block_number_int[0] << '\n' << "Число для 1S-блока в двоичной системе: " << S_block_number_bin[0] << '\n' << '\n';
	out << "Выход S-блоков: " << S_block_out << '\n' << '\n';

	string S_block_out_reverse = P_METHOD(S_block_out);

	//алгоритм Фейстеля
	//вот непосредственно сам алгоритм, сам по себе небольшой

	string StartReversBlocks = "";
	string L0 = "";
	string R0 = "";
	string L_part[QuantityOfRounds];
	string R_part[QuantityOfRounds];
	string LR = "";
	string LR_revers;
	string ResultOfFeistel = "";

	//шифрование
	for (int j = 0; j < NewSizeOfBlock; j++)
	{

		StartReversBlocks = IP_METHOD(Blocks[j]);
		L0 = StartReversBlocks.substr(0, LengthOfBlock / 2);
		R0 = StartReversBlocks.substr(LengthOfBlock / 2, LengthOfBlock / 2);

		out << '\n' << j << "-ая итерация." << '\n' << "L0: " << L0 << '\n' << "R0: " << R0 << '\n';

		for (int i = 0; i < QuantityOfRounds; i++)
		{
			if (i == 0)
			{
				L_part[i] = R0;
				R_part[i] = XORf(L0, f(R0, key48[QuantityOfRounds - 1 - i]));
			}
			else
			{
				L_part[i] = R_part[i - 1];
				R_part[i] = XORf(L_part[i - 1], f(R_part[i - 1], key48[QuantityOfRounds - 1 - i]));
			}
		}

		LR = "";
		LR_revers = "";
		LR = R_part[15] + L_part[15];
		LR_revers = IP_MINUS_METHOD(LR);

		out << '\n' << "Результат для первого блока текста: " << LR_revers << '\n';

		ResultOfFeistel += LR_revers;
	}

	string exit = StringFromBinaryToNormalFormat(ResultOfFeistel);


	help_text = exit;
	ish_text = help_text.c_str();
	UpdateData(FALSE);

	out << '\n' << "Результат шифрования методом DES (BIN): " << '\n' << ResultOfFeistel << '\n' << '\n';
	out << "Длина зашифрованного текста в двоичном виде: " << ResultOfFeistel.length() << '\n' << '\n';
	out << "Результат шифрования методом DES (NORMAL): " << '\n' << exit << '\n' << '\n';
	out << "Длина зашифрованного текста в нормальном виде: " << exit.length() << '\n' << '\n';

	//delete Blocks;
	out.close();
}

struct PRNG
{
	mt19937 engine;
};

void initGenerator(PRNG& generator)
{
	// Создаём псевдо-устройство для получения случайного зерна.
	random_device device;
	// Получаем случайное зерно последовательности
	generator.engine.seed(device());
}

// Генерирует целое число в диапазоне [minValue, maxValue)
unsigned random(PRNG& generator, unsigned minValue, unsigned maxValue)
{
	// Проверяем корректность аргументов
	assert(minValue < maxValue);

	// Создаём распределение
	uniform_int_distribution<unsigned> distribution(minValue, maxValue);

	// Вычисляем псевдослучайное число: вызовем распределение как функцию,
	//  передав генератор произвольных целых чисел как аргумент.
	return distribution(generator.engine);
}

//нахождение НОД алгоритмом Евклида
long long gcd(long long a, long long b) {
	if (b == 0)
		return a;
	return gcd(b, a % b);
}

void gcdext(long long a, long long b, long long* d, long long* x, long long* y)
{
	long long s;
	if (b == 0)
	{
		*d = a; *x = 1; *y = 0;
		return;
	}
	gcdext(b, a % b, d, x, y);
	s = *y;
	*y = *x - (a / b) * (*y);
	*x = s;
}

//быстрое возведение в степень по модулю
long long mul(long long a, long long b, long long m) {
	if (b == 1)
		return a;
	if (b % 2 == 0) {
		long long t = mul(a, b / 2, m);
		return (2 * t) % m;
	}
	return (mul(a, b - 1, m) + a) % m;
}

long long pows(long long a, long long b, long long m) {
	if (b == 0)
		return 1;
	if (b % 2 == 0) {
		long long t = pows(a, b / 2, m);
		return mul(t, t, m) % m;
	}
	return (mul(pows(a, b - 1, m), a, m)) % m;
}

//тест ферма
bool ferma(long long x) {
	if (x == 2)
		return true;
	for (int i = 0; i < 100; i++) {
		long long a = (rand() % (x - 2)) + 2;
		if (gcd(a, x) != 1)
			return false;
		if (pows(a, x - 1, x) != 1)
			return false;
	}
	return true;
}

void CShifratorDlg::RSAShifrator()
{
	UpdateData(TRUE);
	if (open_key == 0) MessageBox(L"Ошибка! Введите или сгенерируйте ключ!", L"ERROR", MB_OK | MB_ICONERROR);

	CString Text = ish_text;
	long long e = open_key;
	long long n = n_glob;

	std::string data;
	data.resize(Text.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Text, -1, &data[0], data.size(), NULL, NULL);

	std::string HexData = "";
	for (unsigned char c : data)
	{
		long long decimalValue = (int)c;
		long long CodeValue = pows(decimalValue, e, n);

		ostringstream stream;
		stream << CodeValue;
		HexData += stream.str() + " ";
	}

	shifr_text = HexData.c_str();
	UpdateData(FALSE);
}

void CShifratorDlg::RSARashifrator()
{
	UpdateData(TRUE);
	if (open_key == 0) MessageBox(L"Ошибка! Введите или сгенерируйте ключ!", L"ERROR", MB_OK | MB_ICONERROR);

	CString Text = shifr_text;

	long long d = 0;
	d = close_key;
	long long n = 0;
	n = n_glob;

	std::string data;
	data.resize(Text.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Text, -1, &data[0], data.size(), NULL, NULL);

	string unshift_data = "";

	int i = 0;
	int j = 0;
	vector<string> arr;
	string sym;
	for (; i < data.length();)
	{
		sym = "";
		stringstream str;
		while (data[i] != ' ')
		{
			str << data[i];
			i++;
		}
		i++;
		str >> sym;

		long long Num = atoi(sym.c_str());
		long long CodeValue = pows(Num, d, n);
		unshift_data += (char)CodeValue;
	}

	ish_text = unshift_data.c_str();
	UpdateData(FALSE);
}

void CShifratorDlg::OnBnClickedKluch()
{
	// TODO: добавьте свой код обработчика уведомлений

	UpdateData(TRUE);
	if (XOR.GetCheck() == BST_CHECKED || DES.GetCheck() == BST_CHECKED)
	{
		int Length = 8;
		char* Key = new char[Length];
		CString helpkey;
		int shetchik_abc = 26;
		int t;

		srand(time(0));
		for (int i = 0; i < Length; i++)
		{
			t = rand() % (shetchik_abc * 2);
			Key[i] = t >= shetchik_abc ? 'a' + t % shetchik_abc : 'A' + t;
			helpkey += Key[i];
		}

		key = helpkey;

		UpdateData(FALSE);
	}

	if (RSA.GetCheck() == BST_CHECKED)
	{
		key = "";

		unsigned int minValue = 256;
		unsigned int maxValue = 65536;
		int count_p = 0;
		int iter_p = 0;

		long long d;
		long long e;
		long long n;

		do
		{
			e = 0;
			d = 0;
			n = 0;

			unsigned int p = 0;
			while (p == 0)
			{
				unsigned int P = rand() % (maxValue - minValue) + minValue;
				if (ferma(P) == true)
				{
					p = 0;
					p = P;
					count_p++;
				}
				else
				{
					count_p = 0;
				}
				iter_p++;
			}

			int count_q = 0;
			int iter_q = 0;

			unsigned int delta = 0;

			unsigned int q = 0;
			while (q == 0)
			{
				delta = 0;
				unsigned int Q = rand() % (maxValue - minValue) + minValue;
				if (p > Q)
				{
					delta = p - Q;
				}
				else
				{
					delta = Q - p;
				}

				if (ferma(Q) == true && Q != p && delta < 5000)
				{
					count_q++;
					q = Q;
				}
				else
				{
					count_q = 0;
				}
				iter_q++;
			}

			// 2. Вычисляем модуль системы n

			n = (long long)p * q;

			// 3. Вычисляем значение функции Эйлера от модуля системы

			long long fi = 0;
			fi = (long long)(p - 1) * (q - 1);

			// 4. Выбираем случайное целое число e < fi(n), удовлетворяющее условию gcd(e, fi(n)) = 1

			int count_e = 0;

			e = rand() % (fi - 0) + 0;
			while (gcd(e, fi) != 1)
			{
				e = rand() % (fi - 0) + 0;
				count_e++;
			}

			// 5. Вычисляем целое число d такое, что ed = 1(mod fi(n))

			long long alpha = 0;
			long long beta = 0;
			long long gcd = 0;
			gcdext(e, fi, &gcd, &alpha, &beta);
			d = alpha;

			ofstream out("Простые числа.txt");
			out << "Простое число P: " << p;
			out << "\nПростое число Q: " << q;
			out << "\n\nМодуль системы n: " << n;
			out << "\n\nЗначение функции Эйлера от модуля системы: " << fi;
			out << "\n\nВыбранное случайное целое число e < fi(n), удовлетворяющее условию gcd(e,fi(n))=1: " << e;
			out << "\n\nНОД по модулю фи: " << gcd % fi;
			out << "\nКоличество итераций для поиска: " << count_e;
			out << "\n\nВычисленное число d: " << d;
			out.close();
		} while (d < 0);
		// 6. (n, e) используются для открытого ключа, d - для закрытого

		open_key = e;
		close_key = d;
		n_glob = 0;
		n_glob = n;

		UpdateData(FALSE);
	}
}


void CShifratorDlg::OnBnClickedZagruzka()
{
	// TODO: добавьте свой код обработчика уведомлений
	if (new_txt.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CFile file;
		file.Open(fileDialog.GetPathName(), CFile::modeRead);

		CStringA str;
		LPSTR Buf = str.GetBuffer(file.GetLength() + 1);
		file.Read(Buf, file.GetLength() + 1);
		Buf[file.GetLength()] = NULL;

		CStringA decod_text = str;

		file.Close();
		str.ReleaseBuffer();

		ish_text = str;
	}

	if (shifrTXT.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CFile file;
		file.Open(fileDialog.GetPathName(), CFile::modeRead);

		CStringA str;
		LPSTR Buf = str.GetBuffer(file.GetLength() + 1);
		file.Read(Buf, file.GetLength() + 1);
		Buf[file.GetLength()] = NULL;

		CStringA decod_text = str;

		file.Close();
		str.ReleaseBuffer();

		shifr_text = str;
	}

	UpdateData(FALSE);
}


void CShifratorDlg::OnBnClickedVypoln()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);

	if (XOR.GetCheck() == BST_CHECKED)
	{
		if (zashifr.GetCheck() == BST_CHECKED)
		{
			XORShifrator();
		}

		if (rasshifr.GetCheck() == BST_CHECKED)
		{
			XORRashifrator();
		}
	}

	if (DES.GetCheck() == BST_CHECKED)
	{
		if (zashifr.GetCheck() == BST_CHECKED)
		{
			DESShifrator();
		}

		if (rasshifr.GetCheck() == BST_CHECKED)
		{
			DESRashifrator();
		}
	}

	if (RSA.GetCheck() == BST_CHECKED)
	{
		if (zashifr.GetCheck() == BST_CHECKED)
		{
			RSAShifrator();
		}

		if (rasshifr.GetCheck() == BST_CHECKED)
		{
			RSARashifrator();
		}
	}
}


void CShifratorDlg::OnBnClickedVygruzka()
{
	// TODO: добавьте свой код обработчика уведомлений
	if (ishod.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CString txt = ish_text;

		CStdioFile f3(fileDialog.GetPathName(), CFile::modeCreate | CFile::modeWrite);

		f3.WriteString(txt);
	}

	if (shifr.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CString txt = shifr_text;

		CStdioFile f3(fileDialog.GetPathName(), CFile::modeCreate | CFile::modeWrite);

		f3.WriteString(txt);
	}

	UpdateData(FALSE);
}
