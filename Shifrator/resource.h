﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Shifrator.rc
//
#define IDD_SHIFRATOR_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_SHYFR                       1001
#define IDC_NESHYFR                     1002
#define IDC_ZAGRUZKA                    1003
#define IDC_VYGRUZKA                    1004
#define IDC_VYPOLN                      1005
#define IDC_SHIFR                       1006
#define IDC_RASHIFR                     1007
#define IDC_KEY                         1008
#define IDC_KLUCH                       1009
#define IDC_SHIFR2                      1010
#define IDC_RASHIFR2                    1011
#define IDC_SHIFR3                      1012
#define IDC_RASHIFR3                    1013
#define IDC_VYGRUZKA2                   1014
#define IDC_XOR                         1015
#define IDC_DES                         1016
#define IDC_RSA                         1017
#define IDC_KEY2                        1019
#define IDC_KEY3                        1020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
