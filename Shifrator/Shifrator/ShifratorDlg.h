﻿
// ShifratorDlg.h: файл заголовка
//

#pragma once

#include <string>

// Диалоговое окно CShifratorDlg
class CShifratorDlg : public CDialogEx
{
// Создание
public:
	CShifratorDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SHIFRATOR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString key;
	afx_msg void OnBnClickedKluch();
	afx_msg void OnBnClickedZagruzka();
	CString ish_text;
	afx_msg void OnBnClickedVypoln();
	CString shifr_text;
	CButton zashifr;
	CButton rasshifr;
	afx_msg void OnBnClickedVygruzka();
	CButton ishod;
	CButton shifr;
	CButton new_txt;
	CButton shifrTXT;
};
