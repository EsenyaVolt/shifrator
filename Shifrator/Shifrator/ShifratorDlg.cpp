﻿
// ShifratorDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Shifrator.h"
#include "ShifratorDlg.h"
#include "afxdialogex.h"
#include <clocale>
#include <iostream>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

// Диалоговое окно CShifratorDlg



CShifratorDlg::CShifratorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SHIFRATOR_DIALOG, pParent)
	, key(_T(""))
	, ish_text(_T(""))
	, shifr_text(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CShifratorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_KEY, key);
	DDX_Text(pDX, IDC_NESHYFR, ish_text);
	DDX_Text(pDX, IDC_SHYFR, shifr_text);
	DDX_Control(pDX, IDC_SHIFR, zashifr);
	DDX_Control(pDX, IDC_RASHIFR, rasshifr);
	DDX_Control(pDX, IDC_SHIFR3, shifr);
	DDX_Control(pDX, IDC_RASHIFR3, ishod);
	DDX_Control(pDX, IDC_RASHIFR2, new_txt);
	DDX_Control(pDX, IDC_SHIFR2, shifrTXT);
}

BEGIN_MESSAGE_MAP(CShifratorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_KLUCH, &CShifratorDlg::OnBnClickedKluch)
	ON_BN_CLICKED(IDC_ZAGRUZKA, &CShifratorDlg::OnBnClickedZagruzka)
	ON_BN_CLICKED(IDC_VYPOLN, &CShifratorDlg::OnBnClickedVypoln)
	ON_BN_CLICKED(IDC_VYGRUZKA, &CShifratorDlg::OnBnClickedVygruzka)
END_MESSAGE_MAP()


// Обработчики сообщений CShifratorDlg

BOOL CShifratorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CShifratorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CShifratorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CShifratorDlg::OnBnClickedKluch()
{
	// TODO: добавьте свой код обработчика уведомлений

	UpdateData(TRUE);

	int Length = 8;
	char* Key = new char[Length];
	CString helpkey;
	int shetchik_abc = 26;
	int t;

	srand(time(0));
	for (int i=0;i<Length;i++)
	{
		t = rand() % (shetchik_abc * 2);
		Key[i] = t >= shetchik_abc ? 'a' + t % shetchik_abc : 'A' + t;
		helpkey += Key[i];
	}

	key = helpkey;

	UpdateData(FALSE);
}


void CShifratorDlg::OnBnClickedZagruzka()
{
	// TODO: добавьте свой код обработчика уведомлений
	if (new_txt.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CFile file;
		file.Open(fileDialog.GetPathName(), CFile::modeRead);

		CStringA str;
		LPSTR Buf = str.GetBuffer(file.GetLength() + 1);
		file.Read(Buf, file.GetLength() + 1);
		Buf[file.GetLength()] = NULL;

		CStringA decod_text = str;

		file.Close();
		str.ReleaseBuffer();

		ish_text = str;
	}

	if (shifrTXT.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CFile file;
		file.Open(fileDialog.GetPathName(), CFile::modeRead);

		CStringA str;
		LPSTR Buf = str.GetBuffer(file.GetLength() + 1);
		file.Read(Buf, file.GetLength() + 1);
		Buf[file.GetLength()] = NULL;

		CStringA decod_text = str;

		file.Close();
		str.ReleaseBuffer();

		shifr_text = str;
	}

	UpdateData(FALSE);
}


void CShifratorDlg::OnBnClickedVypoln()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);
	

	if (zashifr.GetCheck() == BST_CHECKED)
	{
		CString txt = ish_text;
		CString Key = key;

		string str;
		str.resize(txt.GetLength());
		WideCharToMultiByte(CP_ACP, 0, txt, -1, &str[0], str.size(), NULL, NULL);

		string helpkey;
		helpkey.resize(Key.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Key, -1, &helpkey[0], helpkey.size(), NULL, NULL);

		string rezult;
		rezult.resize(txt.GetLength());

		int ost = 0;
		ost = str.length() % helpkey.length();

		int addLength = helpkey.length() - ost;

		int newLength = str.length() + addLength;

		if (ost == 0)
		{
			for (int i = 0; i < str.length(); i++)
			{
				rezult[i] = str[i] ^ helpkey[i % helpkey.length()];
			}
		}
		else
		{
			str.resize(newLength, '0');
			rezult.resize(str.length());

			for (int i = 0; i < newLength; i++)
			{
				rezult[i] = str[i] ^ helpkey[i % helpkey.length()];
			}
		}

		shifr_text = rezult.c_str();

		UpdateData(FALSE);
	}

	if (rasshifr.GetCheck() == BST_CHECKED)
	{
		CString shifr_txt = shifr_text;
		CString Key = key;

		string str;
		str.resize(shifr_txt.GetLength());
		WideCharToMultiByte(CP_ACP, 0, shifr_txt, -1, &str[0], str.size(), NULL, NULL);

		string helpkey;
		helpkey.resize(Key.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Key, -1, &helpkey[0], helpkey.size(), NULL, NULL);

		string rezult;
		rezult.resize(shifr_txt.GetLength());

		for (int i = 0; i < str.length(); i++)
		{
			rezult[i] = str[i] ^ helpkey[i % helpkey.length()];
		}

		for (int i = str.length(); i > str.length()- helpkey.length(); i--)
		{
			if (rezult[i] == '0')
			{
				rezult.erase(i, 1);
			}
		}

		ish_text = rezult.c_str();

		UpdateData(FALSE);
	}
}


void CShifratorDlg::OnBnClickedVygruzka()
{
	// TODO: добавьте свой код обработчика уведомлений
	if (ishod.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CString txt = ish_text;

		CStdioFile f3(fileDialog.GetPathName(), CFile::modeCreate | CFile::modeWrite);

		f3.WriteString(txt);
	}

	if (shifr.GetCheck() == BST_CHECKED)
	{
		setlocale(LC_ALL, "RU");

		CFileDialog fileDialog(TRUE, NULL, L"*.txt");
		int res = fileDialog.DoModal();

		if (res != IDOK) return;

		CString txt = shifr_text;

		CStdioFile f3(fileDialog.GetPathName(), CFile::modeCreate | CFile::modeWrite);

		f3.WriteString(txt);
	}

	UpdateData(FALSE);
}
