﻿
// Shifrator.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CShifratorApp:
// Сведения о реализации этого класса: Shifrator.cpp
//

class CShifratorApp : public CWinApp
{
public:
	CShifratorApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CShifratorApp theApp;
